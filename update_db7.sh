#!/bin/sh
prefix=/usr/local
sql_dir=$prefix/share/tango-db

mysql=/usr/bin/mysql
mysql_config=/etc/sysconfig/tango-db
mysql_host=localhost
mysql_admin_user=root
mysql_admin_passwd=
db_name=tango

echo -n "MySQL host name [$mysql_host]:"
read answer
[[ -n $answer ]] && mysql_host=$answer

echo -n "MySQL admin user name [$mysql_admin_user]:"
read answer
[[ -n $answer ]] && mysql_admin_user=$answer

echo -n "MySQL admin password [$mysql_admin_passwd]:"
read answer
[[ -n $answer ]] && mysql_admin_passwd=$answer

if test "x$mysql_admin_user" = "x"; then
    user_switch="";
else
    user_switch="-u$mysql_admin_user";
fi

if test "x$mysql_admin_passwd" = "x"; then
    passwd_switch="";
else
    passwd_switch="-p$mysql_admin_passwd"
fi

if test "x$mysql_host" = "x"; then
    host_switch="";
else
    host_switch="-h$mysql_host";
fi

$mysql $user_switch $passwd_switch $host_switch < $sql_dir/update_db7.sql > /dev/null

exit $?
